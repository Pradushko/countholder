package com.epam.clickholder

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.epam.clickholder.MainActivity.Companion.COUNTER_VALUE
import kotlinx.android.synthetic.main.fragment_b.*


class FragmentB: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_b, container, false)
        return view
    }

    override fun onStart() {
        super.onStart()
        val bundle = arguments
        if (bundle != null) {
            val counter = bundle.getInt(COUNTER_VALUE)
            updateTextView(counter.toString())
        }
    }

    fun updateTextView(count: String) {
        textView.setText(count)
    }
    companion object {
        val TAG = FragmentB::class.java.name
        fun newInstance(value: Int): FragmentB {
            val fragment = FragmentB()
            val bundle = Bundle()
            bundle.putInt(COUNTER_VALUE, value)
            fragment.arguments = bundle
            return fragment
        }
    }
}