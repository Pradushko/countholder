package com.epam.clickholder

import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

class MainActivity : AppCompatActivity(), FragmentA.OnActivityInteractionListener {

    private var count: Int = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState != null) {
            count = savedInstanceState.getInt(COUNTER_VALUE)
        }
        attachView();
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(COUNTER_VALUE, count)
    }

    private fun attachView() {
        replaceFragment(R.id.fragment_container, FragmentA.newInstance(), FragmentA.TAG, null)
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            replaceFragment(R.id.second_fragment_container, FragmentB.newInstance(count), FragmentB.TAG, null)
        }
    }

    override fun updateCounter() {
        count++
        val fragmentB = supportFragmentManager.findFragmentByTag(FragmentB.TAG) as FragmentB?
        if (fragmentB != null && fragmentB.isVisible) {
            fragmentB.updateTextView(count.toString())
        } else {
            replaceFragment(R.id.fragment_container, FragmentB.newInstance(count), FragmentB.TAG, null)
        }
    }

    private fun replaceFragment(
        containerViewId: Int,
        fragment: Fragment,
        fragmentTag: String,
        backStackStateName: String?
    ) {
        supportFragmentManager.popBackStack()

        val transaction = supportFragmentManager.beginTransaction()
        if (fragment is FragmentB) {
            transaction.addToBackStack(backStackStateName)
        }
        transaction.replace(containerViewId, fragment, fragmentTag)
        transaction.commit()
    }

    companion object {
        const val COUNTER_VALUE = "COUNTER_VALUE"
    }
}
