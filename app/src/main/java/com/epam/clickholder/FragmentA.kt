package com.epam.clickholder

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_a.*

class FragmentA : Fragment() {
    private var listener: OnActivityInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_a, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button.setOnClickListener { listener?.updateCounter() }

    }
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            listener = context as OnActivityInteractionListener?

        } catch (e: ClassCastException) {
            throw ClassCastException(context!!.toString() + " must implement OnActivityInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnActivityInteractionListener {
        fun updateCounter()
    }

    companion object {
        val TAG = FragmentA::class.java.name
        fun newInstance(): FragmentA {
            return FragmentA()
        }
    }
}